
#C99=	c99
#CXX=	c++

CFLAGS=		-Wall -Wextra
CXXFLAGS=	-Wall -Wextra
LDFLAGS=	-Wl,-z,noexecstack -Wl,-z,now -Wl,-z,relro

.SUFFIXES: .cpp .o
.cpp.o:
	${CXX} ${CPPFLAGS} ${CXXFLAGS} -c -o $@ $<

.o:
	${CXX} ${LDFLAGS} -o $@ $<
