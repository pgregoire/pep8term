
include config.mk

BINS=	pep8 asem8


all: ${BINS}

clean:
	rm -f ${BINS} ${BINS:=.o}

## deps
pep8: pep8.o
asem8: asem8.o
